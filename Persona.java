public class Persona{

    String nombre;
    int edad;

    void saluda(){
        System.out.println("Hola me llamo " + this.nombre + " y tengo " + this.edad + " years.");
    }
    void comparaedad(Persona otro){
        if(this.edad < otro.edad){
            int diferencia = otro.edad - this.edad;
            System.out.println("Hola me llamo " + this.nombre + " y tengo " + diferencia + (" años menos que ") + otro.nombre);
        }else if(this.edad > otro.edad){
            int diferenciados = this.edad - otro.edad;
            System.out.println("Hola me llamo " + this.nombre + " y tengo " + diferenciados + (" años más que ") + otro.nombre);
        }
        else{
            System.out.println("Hola me llamo " + this.nombre + " y tengo la misma edad que " + otro.nombre);
        }
    }
    void doblar_edad (){
        System.out.println("Mi edad x2 es " + this.edad*2 + " y mi edad actual es: " + this.edad);
    }

}