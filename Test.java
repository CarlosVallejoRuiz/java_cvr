public class Test{
    public static void main (String[] args){

        
        Persona marta = new Persona();
        marta.nombre = "Marta";
        marta.edad = 25;
        marta.saluda();

        Persona luis = new Persona();
        luis.nombre = "Luis";
        luis.edad = 65;
        luis.comparaedad(marta);

        Persona jose = new Persona();
        jose.nombre = "Jose";
        jose.edad = 15;
        jose.comparaedad(luis);

        Persona nadia = new Persona();
        nadia.nombre = "Nadia";
        nadia.edad = 7;
        nadia.comparaedad(jose);

        Persona jona = new Persona();
        jona.nombre = "Jona";
        jona.edad = 21;
        jona.comparaedad(nadia);
        jona.doblar_edad();
    }
}